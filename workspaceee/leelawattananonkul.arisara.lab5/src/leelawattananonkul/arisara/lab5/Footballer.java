package leelawattananonkul.arisara.lab5;

/*@Author Arisara Leelawattananonkul
 *2018-02-02
 *593040687-9
 */

public class Footballer extends Athlete implements Playable, Movable {

	private static String sport;
	private String position;
	private String team;

	public Footballer(String name, double weight, double height, Gender gender, String nationality, String birthdate,
			String position, String team) {
		super(name, weight, height, gender, nationality, birthdate);
		Footballer.sport = "American Football";
		this.position = position;
		this.team = team;
	}

	public static String getSport() {
		return sport;
	}

	public static void setSport(String sport) {
		Footballer.sport = sport;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	@Override
	public String toString() {
		return "" + super.toString() + ", " + Footballer.sport + ", " + position + ", " + team + "";
	}

	@Override
	public void playSport() {
		System.out.println(getName() + " is good at " + getSport());
	}

	@Override
	public void move() {
		System.out.println(getName() + " moves down the " + getSport().toLowerCase() + " field.");
		
	}

	@Override
	public void play() {
		System.out.println(getName() + " throws a touchdown.");
	}
	
}
